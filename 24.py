import numpy as np
from Bio.SubsMat.MatrixInfo import pam250
from typing import Tuple


def build_dynamic_progr_tbl_4_local_alignment(s: str, t: str, indel_penalty: int = -5) -> Tuple[np.ndarray, np.ndarray]:
    # s to wiersze, t to kolumny
    s = "-" + s
    t = "-" + t
    m = len(s)
    n = len(t)
    d_t = np.zeros((m, n))
    d_t_dir = np.zeros((m, n))
    for i in range(1, m):
        for j in range(1, n):
            try:
                pam_penalty = pam250[(s[i], t[j])]
            except KeyError:
                pam_penalty = pam250[(t[j], s[i])]
            options = {
                0: 0,
                3: d_t[i - 1][j] + indel_penalty,  # up
                1: d_t[i][j - 1] + indel_penalty,  # left
                2: d_t[i - 1][j - 1] + pam_penalty  # diagonal
            }
            # print(i, j)
            # print(options)
            # print(max(options, key=options.get))
            d_t_dir[i][j] = max(options, key=options.get)
            d_t[i][j] = options[max(options, key=options.get)]
    return d_t, d_t_dir


def get_max_alignment_score(d_t: np.ndarray) -> Tuple[int, int, int]:
    max_align_score = np.max(d_t)

    row, col = np.where(d_t == max_align_score)
    row = row[0]  # there can be multiple best substrings and as I can output any, I choose first one...
    col = col[0]
    return int(max_align_score), row, col


def get_substrings_with_max_score(d_t_dir: np.ndarray, row: int, col: int, s: str, t: str) -> Tuple[str, str]:
    r = ""  # substring of s
    u = ""  # substring of t
    while True:
        direction = d_t_dir[row][col]
        if direction == 2:  # diagonal
            row -= 1
            col -= 1
            r += s[row]
            u += t[col]
        elif direction == 1:  # left
            col -= 1
            u += t[col]
        elif direction == 3:  # up
            row -= 1
            r += s[row]
        elif direction == 0:
            break

    return r[::-1], u[::-1]


DNAs = dict()
current_DNA_hlpr = ""
with open('./data/rosalind_loca.txt', 'r') as file:
    for line in file:
        if line[0] == ">":
            current_DNA_hlpr = line[1:-1]
            DNAs[current_DNA_hlpr] = ""
        else:
            DNAs[current_DNA_hlpr] += line[:-1]

DNAs = [value for value in DNAs.values()]
dyn_tbl, dyn_tbl_dir = build_dynamic_progr_tbl_4_local_alignment(DNAs[0], DNAs[1])
max_alig_score, x, y = get_max_alignment_score(dyn_tbl)
r, u = get_substrings_with_max_score(dyn_tbl_dir, x, y, DNAs[0], DNAs[1])
# print(dyn_tbl)
# print(dyn_tbl_dir)
print(max_alig_score)
print(r)
print(u)

