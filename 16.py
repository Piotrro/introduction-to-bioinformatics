from math import log10


def calculate_probability(sequence: str, gc_content: float) -> float:
    result = 0
    for character in sequence:
        if character in "GC":
            result += log10(gc_content / 2)
        else:
            result += log10((1 - gc_content) / 2)
    return result


with open('./data/rosalind_prob.txt', 'r') as file:
    s = file.readline()[:-1]
    vector_A = file.readline()[:-1].split()

gc_contents = [float(element) for element in vector_A]
for value in [calculate_probability(s, gc_content) for gc_content in gc_contents]:
    print(f"{value:.3f}", end=" ")

