def p_distance(s1: str, s2: str) -> float:
    if len(s1) != len(s2):
        raise Exception("Two strings should be same length to calculate p-distance!")
    distance = 0
    for i in range(len(s1)):
        if s1[i] != s2[i]:
            distance += 1
    return distance / len(s1)


DNAs = dict()
current_DNA_hlpr = ""
with open('./data/rosalind_pdst.txt', 'r') as file:
    for line in file:
        if line[0] == ">":
            current_DNA_hlpr = line[1:-1]
            DNAs[current_DNA_hlpr] = ""
        else:
            DNAs[current_DNA_hlpr] += line[:-1]

DNAs = [value for value in DNAs.values()]
n = len(DNAs)
distance_matrix = [[p_distance(DNAs[i], DNAs[j]) for j in range(n)] for i in range(n)]
for i in range(n):
    for value in distance_matrix[i]:
        print(f"{value:.5f}", end=" ")
    print()

