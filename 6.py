with open('./data/rosalind_ini6.txt', 'r') as file:
    s = file.readline()

words = s.split()
counter = {word: 0 for word in words}
for word in words:
    counter[word] += 1

for word, number in counter.items():
    print(word, number)