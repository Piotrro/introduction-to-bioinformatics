with open('./data/rosalind_ini3.txt', 'r') as file:
    s = file.readline()
    coor = [int(num) for num in file.readline().split()]

print(s[coor[0]:coor[1] + 1] + " " + s[coor[2]:coor[3] + 1])
