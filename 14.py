from typing import List


def get_k_mers(k: int, alphabet: str) -> List[str]:
    result = list()
    alp_len = len(alphabet)
    for i in range(alp_len ** k):
        tmp_str = ""
        for j in range(k):
            tmp_str += alphabet[int(i / alp_len ** j) % alp_len]
        result.append(tmp_str[::-1])  # reverse to keep alphabetical order
    return result


with open('./data/rosalind_lexf.txt', 'r') as file:
    alphabet = file.readline()[:-1].replace(" ", "")
    k = int(file.readline())

for combination in get_k_mers(k, alphabet):
    print(combination)
