>Rosalind_2761
IHVCFYNITILRPYDWERSQDFYSAPPHGKRIWCDSLMNEGSQIKASDTTTSGGEQLNVM
IEYETWLPIWQDITDIGLIWWWMTTLDALAHCDIATMEESGMAFPSVEVHPCDERLWFDK
MNWVDMQQDLNFFTTPARPNDPDCDNEYVPNPKCPKNCIRKQIDNIQRTWTATTIQCKYN
ATMGHMAMWHKHHRFNEFHCMLCEWNLQIIINFRMRRNPTFHPMLAVLEQHMVFRQRFWA
KRRRAQERMKRADILISIMPAGENFHDYLKWREVIPEMALTGDCWVHEIQADDQLAVKRA
RQQKTSTYQCIFYSANPMGGKINETPRLTDFAEKTTKCQWKHRKWRFCIAWDELQMVAFR
QQMWPNCIGGQKWDETMIQQVGIQFHLYIFPLHQLGFYSKHNGTLPMVYLHRGNVTTWSM
FIQRPTSSFRQDYVIAKYHIVWEYHIKVQQLGCSQDWCGLCEQFTIWDERVGDSNIKHEQ
ANFQQTAPGPEAVGDQAIDIRKDCFHINMEQDFFDIETEIYLIEHDRYDHAISTTHYLLM
KESTLQCPRYHSFSAQPQGQQQRECKKKIYMFFNESVFDFHGWIISFATTRKVLYTLWYR
PTWYGMYQHNQMNWDPKINYNMVWVQIHPPGMRQGMMFWPTMIWAVCDDEQTEGLKTEGA
NMKYTEPKIPGIYIWGMFNKGEHGDPSRAFAVWPCKQVWYSHGVCWTFYTPWNMHHFHHM
GVQFNVWEWWMVNSRQTRHMGVISISMICFWDAESWCHGNKHSTSRYDQMGPDEAVENSV
WLLEVENYKFKCFAWAHIQVWKTSEYLHDYEYACPWSKDPQIPETAFNAYCFPYHPASIT
MWWEWKDGIHCIKSQSIHDDVKPTV
>Rosalind_2598
IIMDQAMATRNRYYYHGKRIYCDSLMFEGLMLQIKAMDTTTCCMGRGGEQLNKFYGSFQM
IWAPIWQDITRIGAGQPDIWWWMTTLKIATMEESGMYFPSVETHPADPRLWFDKMNWVDM
FRPNDIMPDCDNEYVPNPKCPKNCSQSIRVNNRVVSPQEDHKTYIWTATTIQCKFNATMG
MSFPFPMAMWHKWHRNCFHCMLCEWNLQIIINFRMRRNPHQGDGAMMFHPLFFRTAVLAI
SQCDLFWWWRVPNMNKRRERAQERMNEIRAHILISAWEFQVGMDVHDYLKWREVIPEMAL
TPIDPWLMQADDQLAVKRARIYNGICIFGQVVYYCKVSANPMGGKINETPRLTDFAEECQ
WKHRKWRFCIVIFHKACRIFRQQMWPNCIGGQGRKGVGIQQVGIQKHLYIFPHNGTLPMV
YLHFICRPTSSFRQDYWEYHCKWLYQCGSCENIKHEQANFQAAATLRTAPGPEADIRKDC
FHIHMEQDFFMIETENYLIEHTNDKRNNPCNFRNDHAISTTHYLLMIESTRYPSFSAEWP
QGQQQTSIEILVFDFHGFATTRNVLYTLWYNVTWYGMNVQHINYNSRKSGQVWVQIHFMP
GMRQGYHFWPTMIWAVCDDMQTEGLLLVDSRTEGANMKYTIAHDSKIPWGMFNKGETGAF
AVWPCKQPVWWYSQKCWTFYPPNNMHHFHEMGVQFNVWVWPGTMIMVNSRQTRHDWIRTG
YSVIHHPATTQDPNSMICFWDKHSTSRYDQMGGPLEAWAHIQVWYTSEYLHDSKMVSNPK
DQFNGASSISGETASNAYCFPYWNFHPASITMWWEWKDGIHCIKSQKCGIHMDYYECGVK
YWPTPTV
