with open('./data/rosalind_ini2.txt', 'r') as file:
    integers = [[int(num) for num in line.split()] for line in file]

sum = 0
for number in integers[0]:
    sum += number ** 2
print(sum)