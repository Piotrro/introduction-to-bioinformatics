from typing import Dict, List


def read_fasta_file(path: str) -> Dict[str, str]:
    DNAs = dict()
    current_DNA_hlpr = ""
    with open(path, 'r') as file:
        for line in file:
            if line[0] == ">":
                current_DNA_hlpr = line[1:-1]
                DNAs[current_DNA_hlpr] = ""
            else:
                DNAs[current_DNA_hlpr] += line[:-1]
    return DNAs


def remove_introns(sequence: str, introns: List[str]) -> str:
    for intron in introns:
        sequence = sequence.replace(intron, "")
    return sequence


rna_codon = {"UUU": "F", "CUU": "L", "AUU": "I", "GUU": "V",
             "UUC": "F", "CUC": "L", "AUC": "I", "GUC": "V",
             "UUA": "L", "CUA": "L", "AUA": "I", "GUA": "V",
             "UUG": "L", "CUG": "L", "AUG": "M", "GUG": "V",
             "UCU": "S", "CCU": "P", "ACU": "T", "GCU": "A",
             "UCC": "S", "CCC": "P", "ACC": "T", "GCC": "A",
             "UCA": "S", "CCA": "P", "ACA": "T", "GCA": "A",
             "UCG": "S", "CCG": "P", "ACG": "T", "GCG": "A",
             "UAU": "Y", "CAU": "H", "AAU": "N", "GAU": "D",
             "UAC": "Y", "CAC": "H", "AAC": "N", "GAC": "D",
             "UAA": "", "CAA": "Q", "AAA": "K", "GAA": "E",
             "UAG": "", "CAG": "Q", "AAG": "K", "GAG": "E",
             "UGU": "C", "CGU": "R", "AGU": "S", "GGU": "G",
             "UGC": "C", "CGC": "R", "AGC": "S", "GGC": "G",
             "UGA": "", "CGA": "R", "AGA": "R", "GGA": "G",
             "UGG": "W", "CGG": "R", "AGG": "R", "GGG": "G"
             }

DNAs = read_fasta_file("data/rosalind_splc.txt")
DNAs = [value for value in DNAs.values()]
sequence_without_introns = remove_introns(DNAs[0], DNAs[1:])
sequence_without_introns = sequence_without_introns.replace("T", "U")
codons = [sequence_without_introns[i:i+3] for i in range(0, len(sequence_without_introns)-1, 3)]

output = ""
for codon in codons:
    output += rna_codon[codon]
print(output)

