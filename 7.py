with open('./data/rosalind_dna.txt', 'r') as file:
    s = file.readline()

alphabet = "ACGT"
counter = {letter: s.count(letter) for letter in alphabet}
for letter in alphabet:
    print(counter[letter], end=' ')
