from typing import Dict


def read_fasta_file(path: str) -> Dict[str, str]:
    DNAs = dict()
    current_DNA_hlpr = ""
    with open(path, 'r') as file:
        for line in file:
            if line[0] == ">":
                current_DNA_hlpr = line[1:-1]
                DNAs[current_DNA_hlpr] = ""
            else:
                DNAs[current_DNA_hlpr] += line[:-1]
    return DNAs


class ConsensusAndProfile:
    def __init__(self, DNA_string_lenth: int):
        self.DNA_string_lenth = DNA_string_lenth
        self.ACGT = {
            "A": [0] * DNA_string_lenth,
            "C": [0] * DNA_string_lenth,
            "G": [0] * DNA_string_lenth,
            "T": [0] * DNA_string_lenth
            }

    def print_consensus(self) -> None:
        for i in range(self.DNA_string_lenth):
            max_count_and_symbol = (0, "A")
            for key in self.ACGT.keys():
                if self.ACGT[key][i] > max_count_and_symbol[0]:
                    max_count_and_symbol = (self.ACGT[key][i], key)
            print(max_count_and_symbol[1], end='')
        print()

    def print_profile(self) -> None:
        for key, value in self.ACGT.items():
            print(key + ":", end="")
            for element in value:
                print(" " + str(element), end="")
            print()


DNAs = read_fasta_file('./data/rosalind_cons.txt')

DNA_strings = [value for value in DNAs.values()]


c_p = ConsensusAndProfile(len(DNA_strings[0]))
for dna in DNA_strings:
    for index in range(len(dna)):
        c_p.ACGT[dna[index]][index] += 1

c_p.print_consensus()
c_p.print_profile()
print()
