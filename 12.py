import re


with open('./data/rosalind_subs.txt', 'r') as file:
    s = file.readline()[:-1]  # the string
    t = file.readline()[:-1]  # the substring
# the -1 removes enters...

positions = [match.start() + 1 for match in re.finditer(f'(?={t})', s)]
for position in positions:
    print(position, end=" ")
