from typing import Dict, List


def read_fasta_file(path: str) -> Dict[str, str]:
    DNAs = dict()
    current_DNA_hlpr = ""
    with open(path, 'r') as file:
        for line in file:
            if line[0] == ">":
                current_DNA_hlpr = line[1:-1]
                DNAs[current_DNA_hlpr] = ""
            else:
                DNAs[current_DNA_hlpr] += line[:-1]
    return DNAs


def revert_genome(sequence: str) -> str:
    reverted = sequence[::-1]
    reverted = reverted.replace("G", "c").replace("C", "g").replace("A", "t").replace("T", "a").upper()
    return reverted


def find_palindromes(sequence: str, min_len: int = 4, max_len: int = 12) -> List[List[int]]:
    pairs = list()
    for i in range(len(sequence) - min_len + 1):
        for j in range(min_len, max_len + 1, 1):
            if i+j > len(sequence):
                break
            if sequence[i:i+j] == revert_genome(sequence[i:i+j]):
                pairs.append([i + 1, j])
    return pairs


DNAs = read_fasta_file("data/rosalind_revp.txt")
dna = DNAs[next(iter(DNAs))]
pairs = find_palindromes(dna)
for pair in pairs:
    print(f"{pair[0]} {pair[1]}")

