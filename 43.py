from typing import List


def lcs(part: str, sequences: List[str]) -> bool:
    if len(sequences) < 1 and len(part) < 1:
        return False
    for i in range(len(sequences)):
        if part not in sequences[i]:
            return False
    return True


def lcs_wraper(sequences: List[str]) -> str:
    sub = ''
    if len(sequences) > 1 and len(sequences[0]) > 0:
        for i in range(len(sequences[0])):
            for j in range(len(sequences[0]) - i + 1):
                if j > len(sub) and lcs(sequences[0][i:i + j], sequences):
                    sub = sequences[0][i:i + j]
    return sub


DNAs = dict()
current_DNA_hlpr = ""
with open('./data/rosalind_lcsm.txt', 'r') as file:
    for line in file:
        if line[0] == ">":
            current_DNA_hlpr = line[1:-1]
            DNAs[current_DNA_hlpr] = ""
        else:
            DNAs[current_DNA_hlpr] += line[:-1]
DNAs = [value for value in DNAs.values()]

print(lcs_wraper(DNAs))
