with open('./data/rosalind_revc.txt', 'r') as file:
    s = file.readline()

s = s[::-1]  # reverse
s = s.replace("A", "t").replace("T", "a").replace("C", "g").replace("G", "c")
print(s.upper())
