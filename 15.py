from typing import List


def compute_failure_array(sequence: str) -> List[int]:
    n = len(sequence)
    p = [0] * n
    k = 0
    for i in range(1, n):
        while k > 0 and sequence[i] != sequence[k]:
            k = p[k - 1]
        if sequence[k] == sequence[i]:
            k += 1
        p[i] = k
    return p


DNAs = dict()
current_DNA_hlpr = ""
with open('./data/rosalind_kmp.txt', 'r') as file:  # kmp
    for line in file:
        if line[0] == ">":
            current_DNA_hlpr = line[1:-1]
            DNAs[current_DNA_hlpr] = ""
        else:
            DNAs[current_DNA_hlpr] += line[:-1]

DNA = DNAs[current_DNA_hlpr]
for element in compute_failure_array(DNA):
    print(element, end=" ")
