from Bio import SeqIO
from typing import List


def revert_genome(sequence: str) -> str:
    reverted = sequence[::-1]
    reverted = reverted.replace("G", "c").replace("C", "g").replace("A", "t").replace("T", "a").upper()
    return reverted


def generate_codon_sequences(sequence: str) -> List[List[str]]:
    result = list()
    for j in range(3):
        result.append([sequence[i+j:i+3+j] for i in range(0, len(sequence), 3)])
        if len(result[j][-1]) < 3:
            del result[j][-1]
    return result


def find_genes_in_codons_sequence(codon_sequence: List[str]) -> List[List[str]]:
    result = list()
    for position, codon in enumerate(codon_sequence):
        if codon == "ATG":  # if the codon is a "start"
            tmp = [codon]
            is_to_add = False
            for possible_end_codon in codon_sequence[position + 1:]:
                if possible_end_codon in ["TAA", "TAG", "TGA"]:  # if the codon is "end"
                    is_to_add = True
                    break
                tmp.append(possible_end_codon)
            if is_to_add:
                result.append(tmp)
    return result


rna_codon = {"UUU": "F", "CUU": "L", "AUU": "I", "GUU": "V",
             "UUC": "F", "CUC": "L", "AUC": "I", "GUC": "V",
             "UUA": "L", "CUA": "L", "AUA": "I", "GUA": "V",
             "UUG": "L", "CUG": "L", "AUG": "M", "GUG": "V",
             "UCU": "S", "CCU": "P", "ACU": "T", "GCU": "A",
             "UCC": "S", "CCC": "P", "ACC": "T", "GCC": "A",
             "UCA": "S", "CCA": "P", "ACA": "T", "GCA": "A",
             "UCG": "S", "CCG": "P", "ACG": "T", "GCG": "A",
             "UAU": "Y", "CAU": "H", "AAU": "N", "GAU": "D",
             "UAC": "Y", "CAC": "H", "AAC": "N", "GAC": "D",
             "UAA": "STOP", "CAA": "Q", "AAA": "K", "GAA": "E",
             "UAG": "STOP", "CAG": "Q", "AAG": "K", "GAG": "E",
             "UGU": "C", "CGU": "R", "AGU": "S", "GGU": "G",
             "UGC": "C", "CGC": "R", "AGC": "S", "GGC": "G",
             "UGA": "STOP", "CGA": "R", "AGA": "R", "GGA": "G",
             "UGG": "W", "CGG": "R", "AGG": "R", "GGG": "G"
             }

with open('data/rosalind_orf.txt', 'r') as handle:
    genome_seq = str(SeqIO.read(handle, 'fasta').seq)

codon_seqs = generate_codon_sequences(genome_seq) + generate_codon_sequences(revert_genome(genome_seq))
codon_seqs = [find_genes_in_codons_sequence(codon_seq) for codon_seq in codon_seqs]
all_codon_seqs = list()
for asd in codon_seqs:
    all_codon_seqs += asd
all_protein_seqs = list()
for element in all_codon_seqs:
    all_protein_seqs.append("".join([rna_codon[codon.replace("T", "U")] for codon in element]))
all_protein_seqs = set(all_protein_seqs)

for protein_seq in all_protein_seqs:
    print(protein_seq)
