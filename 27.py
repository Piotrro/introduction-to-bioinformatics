from typing import Dict, List, Tuple


def read_HMM_file_27(file_path: str) -> Tuple[str, Dict[str, int], List[List[float]]]:
    hidden_path = ""
    states = dict()
    transition_matrix = list()
    with open(file_path, 'r') as file:
        line_number = -1
        prob_row_count = -1
        for line in file:
            line_number += 1
            if line_number == 0:
                hidden_path = line[0:-1]
            if line_number == 2:
                states_list = line.split()
                count = 0
                for state in states_list:
                    states[state] = count
                    count += 1
                # transition_matrix = numpy.zeros((len(states_list),len(states_list)))
            if line_number > 4:
                prob_row_count += 1
                # transition_matrix[prob_row_count] = line.split()
                transition_matrix.append(list(map(float, line[1:].split())))
    return hidden_path, states, transition_matrix


hidden_path, states, transition_matrix = read_HMM_file_27('./data/rosalind_ba10a.txt')

probability_of_path = 0.5

previous_state = None
for state in list(hidden_path):
    if previous_state is not None:
        probability_of_path *= transition_matrix[states[previous_state]][states[state]]
    previous_state = state

print(probability_of_path)
