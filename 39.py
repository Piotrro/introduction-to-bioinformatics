from typing import Dict


def read_fsta_file(path: str) -> Dict[str, str]:
    DNAs = dict()
    current_DNA_hlpr = ""
    with open(path, 'r') as file:
        for line in file:
            if line[0] == ">":
                current_DNA_hlpr = line[1:-1]
                DNAs[current_DNA_hlpr] = ""
            else:
                DNAs[current_DNA_hlpr] += line[:-1]
    return DNAs


def is_transition(nuc_first: str, nuc_second: str) -> bool:
    # A<->G
    # C<->T    
    translations = {"A": "G", "G": "A", "C": "T", "T": "C"}
    # print(nuc_first + " " + nuc_second+"\n")
    return translations[nuc_first] == nuc_second


def transition_transversion_ratio(s: str, t: str) -> float:
    transition_counter = 0
    transversion_counter = 0
    for i in range(len(s)):
        # print(str(s[i] != t[i]) + " " +s[i] + " " + t[i] )
        if s[i] != t[i]:
            if is_transition(s[i], t[i]):
                transition_counter += 1
            else:
                transversion_counter += 1
    return transition_counter / transversion_counter


DNAs = read_fsta_file('./data/rosalind_tran.txt')

DNA_strings = [value for value in DNAs.values()]
print(transition_transversion_ratio(DNA_strings[0], DNA_strings[1]))





