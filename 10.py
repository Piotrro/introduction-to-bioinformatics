def calculate_gc_content_factor(dna: str) -> float:
    return ((dna.count('C') + dna.count('G')) / len(dna)) * 100


DNAs = dict()
current_DNA_hlpr = ""
with open('./data/rosalind_gc.txt', 'r') as file:
    for line in file:
        if line[0] == ">":
            current_DNA_hlpr = line[1:-1]
            DNAs[current_DNA_hlpr] = ""
        else:
            DNAs[current_DNA_hlpr] += line[:-1]

GC_contents = {id: calculate_gc_content_factor(dna) for id, dna in DNAs.items()}
best_one = max(GC_contents, key=GC_contents.get)
print(f"{best_one}\n{GC_contents[best_one]}")
