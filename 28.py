from typing import Tuple, Dict, List


def read_HMM_file_28(path: str) -> Tuple[str, Dict[str, int], str, Dict[str, int], List[List[float]]]:
    x = ""
    alphabet = dict()
    hidden_path = ""
    states = dict()
    emission_matrix = list()
    with open(path, 'r') as file:
        line_number = -1
        prob_row_count = -1
        for line in file:
            line_number += 1
            if line_number == 0:
                x = line[0:-1]
            elif line_number == 2:
                alphabet_list = line.split()
                letter_number = 0
                for letter in alphabet_list:
                    alphabet[letter] = letter_number
                    letter_number += 1
            elif line_number == 4:
                hidden_path = line[0:-1]
            elif line_number == 6:
                states_list = line.split()
                state_number = 0
                for state in states_list:
                    states[state] = state_number
                    state_number += 1
            elif line_number > 8:
                prob_row_count += 1
                emission_matrix.append(list(map(float, line[1:].split())))
    return x, alphabet, hidden_path, states, emission_matrix


x, alphabet, hidden_path, states, emission_matrix = read_HMM_file_28('./data/rosalind_ba10b.txt')

probability = 1

# print(emission_matrix[alphabet["y"]][states["B"]])

for i in range(len(x)):
    probability *= emission_matrix[states[hidden_path[i]]][alphabet[x[i]]]

print(probability)











