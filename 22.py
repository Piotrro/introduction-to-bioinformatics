import numpy as np
from typing import List, Tuple


def edit_distance(s: str, t: str) -> Tuple[int, np.ndarray]:
    m = len(s) + 1
    n = len(t) + 1
    dynamic_table = np.zeros((m, n))
    for i in range(m):
        for j in range(n):
            if i == 0:
                dynamic_table[i][j] = j
            elif j == 0:
                dynamic_table[i][j] = i
            elif s[i - 1] == t[j - 1]:
                dynamic_table[i][j] = dynamic_table[i - 1][j - 1]
            else:
                dynamic_table[i][j] = 1 + min(dynamic_table[i - 1][j - 1],
                                              dynamic_table[i][j - 1],
                                              dynamic_table[i - 1][j])
    return int(dynamic_table[m-1][n-1]), dynamic_table


def make_best_align_strings(s: str, t: str, dynamic_table: np.ndarray, indel: str = "-") -> Tuple[str, str]:
    s_out = ""
    t_out = ""
    m = len(s)
    n = len(t)

    while m > 0 and n > 0:
        options = {
            "diag": dynamic_table[m - 1][n - 1],
            "up": dynamic_table[m - 1][n],
            "left": dynamic_table[m][n - 1]
        }
        move = min(options, key=options.get)
        # print(options, end=" ")  # debug
        # print(move)  # debug

        if move == "diag":
            s_out += s[m - 1]
            t_out += t[n - 1]
            m -= 1
            n -= 1
        elif move == "up":
            s_out += s[m - 1]
            t_out += indel
            m -= 1
        elif move == "left":
            s_out += indel
            t_out += t[n - 1]
            n -= 1

    return s_out[::-1], t_out[::-1]


def hamming_distance(s, t):  # for debugging purposes
    counter = 0
    for i in range(len(s)):
        if s[i] != t[i]:
            counter += 1
    return counter


DNAs = dict()
current_DNA_hlpr = ""
with open('./data/rosalind_edta.txt', 'r') as file:
    for line in file:
        if line[0] == ">":
            current_DNA_hlpr = line[1:-1]
            DNAs[current_DNA_hlpr] = ""
        else:
            DNAs[current_DNA_hlpr] += line[:-1]

DNAs = [value for value in DNAs.values()]
distance, d_t = edit_distance(DNAs[0], DNAs[1])
print(distance)
new_s, new_t = make_best_align_strings(DNAs[0], DNAs[1], d_t)
print(new_s)
print(new_t)
