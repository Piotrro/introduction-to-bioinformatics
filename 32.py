from typing import List, Tuple


def revert_seq(sequence: str) -> str:
    """
    Changes the genome sequence to its "second string" by reverting the order of nucleotides, and then changing each
    nucleotide to its "mirror brother".
    :param sequence: string describing genome in ACGT alphabet.
    :return: string representing the "second string" of DNA, in ACGT alphabet.
    """
    reverted = sequence[::-1]
    reverted = reverted.replace("G", "c").replace("C", "g").replace("A", "t").replace("T", "a").upper()
    return reverted


def make_graph_pairs(seqs: List[str]) -> List[Tuple[str, str]]:
    output = list()
    for seq in seqs:
        output.append((seq[:-1], seq[1:]))
    return output


sequences = list()
with open("data/rosalind_dbru.txt", "r") as file:
    for line in file:
        sequences.append(line[:-1])

rev_sequences = [revert_seq(sequence) for sequence in sequences]
all_unique_seqs = list(set(sequences + rev_sequences))
all_unique_seqs.sort()
for pair in make_graph_pairs(all_unique_seqs):
    # print(pair)
    print(f"({pair[0]}, {pair[1]})")
