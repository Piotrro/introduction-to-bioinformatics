with open('./data/rosalind_ini4.txt', 'r') as file:
    domain = [int(num) for num in file.readline().split()]
sum = 0
for i in range(domain[0], domain[1] + 1):
    if i % 2 == 1:
        sum += i

print(sum)