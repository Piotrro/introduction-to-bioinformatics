from math import log as ln
from matplotlib import pyplot as plt
from typing import Dict, Optional, Tuple
from scipy.cluster.hierarchy import dendrogram, linkage
from Bio.Phylo.TreeConstruction import DistanceTreeConstructor, DistanceMatrix
from Bio.Phylo import draw
import numpy as np
from tqdm import tqdm


class Node:
    """
    Class for building a tree data structure
    """
    def __init__(self, name: str, left_child: Optional['Node'] = None, right_child: Optional['Node'] = None) -> None:
        self.name = name
        self.left_child = left_child
        self.right_child = right_child

    def __str__(self):
        if self.left_child is None:
            return f"leaf-Node({self.name})"
        else:
            return f"Node({self.name} -> {self.left_child}, {self.right_child})"


def align_global(s1: str, s2: str, same_penalty: Optional[int] = None, differ_penalty: Optional[int] = None,
                 indel_penalty: Optional[int] = None) -> Tuple[str, str]:
    """
    Gets two strings and returns their global alignments.

    :param s1:
    :param s2:
    :param same_penalty: penalty (or rather reward) for adding same characters to both strings at the same positions
    :param differ_penalty: penalty for adding two different characters to both strings at the same position
    :param indel_penalty: linear penalty for adding an indel ("-") to any of strings s or t
    :return: two strings aligned to each other
    """

    if same_penalty is None:
        same_penalty = 1
    if differ_penalty is None:
        differ_penalty = -1
    if indel_penalty is None:
        indel_penalty = -2

    _, dyn_tbl_dir = scoring_matrix_distance(s1, s2, same_penalty, differ_penalty, indel_penalty)
    return get_aligned_strings(dyn_tbl_dir, s1, s2)


def scoring_matrix_distance(s: str, t: str, same_penalty: int, differ_penalty: int,
                            indel_penalty: int) -> Tuple[np.ndarray, np.ndarray]:
    """
    Creates dynamic programming table for global alignment task. It also produces matrix containing integers decoding
    direction from where the current result was calculated (1 - from left cell, 3 - from right cell, 2 - from diagonal
    cell).

    :param s:
    :param t:
    :param same_penalty: penalty (or rather reward) for adding same characters to both strings at the same positions
    :param differ_penalty: penalty for adding two different characters to both strings at the same position
    :param indel_penalty: linear penalty for adding an indel ("-") to any of strings s or t
    :return: dynamic programing table, and matrix with directions
    """
    # s to wiersze, t to kolumny
    s = "-" + s
    t = "-" + t
    m = len(s)
    n = len(t)
    d_t = np.zeros((m, n))
    d_t_dir = np.zeros((m, n))
    for i in range(m):
        for j in range(n):
            if i == 0:
                d_t[i][j] = j * indel_penalty
                if j != 0:
                    d_t_dir[i][j] = 1
            elif j == 0:
                d_t[i][j] = i * indel_penalty
                if i != 0:
                    d_t_dir[i][j] = 3
            else:
                penalty = same_penalty
                if s[i] != t[j]:
                    penalty = differ_penalty
                options = {
                    1: d_t[i][j - 1] + indel_penalty,  # left
                    2: d_t[i - 1][j - 1] + penalty,  # diagonal
                    3: d_t[i - 1][j] + indel_penalty  # up
                }
                d_t_dir[i][j] = max(options, key=options.get)
                d_t[i][j] = options[max(options, key=options.get)]
    # print(s, t)  # debug
    # print(d_t)
    # print(d_t_dir)
    return d_t, d_t_dir


def get_aligned_strings(d_t_dir: np.ndarray, s: str, t: str) -> Tuple[str, str]:
    """
    Performs backtracking in dynamic programming table (to be more precise: backtracking of the matrix containing
    directions from where current cell's result was calculated). In other words it produces aligned sequences from not
    aligned sequences on the basis of matix of "jumps".

    :param d_t_dir:
    :param s:
    :param t:
    :return: two strings aligned to each other
    """
    r = ""  # substring of s
    u = ""  # substring of t
    row = len(s)
    col = len(t)
    while True:
        direction = d_t_dir[row][col]
        if direction == 2:  # diagonal
            row -= 1
            col -= 1
            r += s[row]
            u += t[col]
        elif direction == 1:  # left
            col -= 1
            u += t[col]
            r += "-"
        elif direction == 3:  # up
            row -= 1
            r += s[row]
            u += "-"
        elif direction == 0:
            break
    return r[::-1], u[::-1]


def calculate_jc_distance(gene_1: str, gene_2: str) -> float:
    """
    Calculates phylogenetic distance between two gene sequences using the Jukes-Cantor formula (it aligns the two genes
    before calculation).

    :param gene_1:
    :param gene_2:
    :return: the distance
    """
    aligned_gene_1, aligned_gene_2 = align_global(gene_1, gene_2)
    numof_changes = 0
    # print(aligned_gene_1, aligned_gene_2)
    for i in range(len(aligned_gene_1)):
        if aligned_gene_1[i] != aligned_gene_2[i]:
            numof_changes += 1
    d = numof_changes / len(aligned_gene_1)
    distance = (-3 / 4) * ln(1 - ((4 / 3) * d))
    return distance


def upgma(gene_variations: Dict[int, str], species: Dict[int, str]):
    """
    Performs UPGMA algorithm on given gene variations and builds the phylogenetic tree. It also measure how good the
    tree is, by calculating the distance between root and first nontreminal node in the tree (only if second ancestor
    of the root is Danio rerio - else the score is -1, as the tree poorly divide the species into mammals and fish).

    :param gene_variations:
    :param species:
    :return:
    """
    # gene_variations = {0: "aaaaaaaa", 1: "aaaaaass", 2: "aaaassdd", 3: "aassgggg", 4: "aasshhhh", 5: "sskkllll"}
    # species = {0: "A", 1: "B", 2: "C", 3: "D", 4: "E", 5: "F"}
    decoder = {i: gene_id for i, gene_id in enumerate(gene_variations)}
    n = len(gene_variations)
    names = [species[gene_id] for gene_id in gene_variations.keys()]
    matrix = list()
    for i in range(n):
        row = list()
        for j in range(0, i):
            row.append(calculate_jc_distance(gene_variations[decoder[j]], gene_variations[decoder[i]]))
        row.append(0.0)
        matrix.append(row)

    dm = DistanceMatrix(names, matrix)
    # print(dm)
    constructor = DistanceTreeConstructor()
    tree = constructor.upgma(dm)
    # print(tree)
    ancestors = tree.clade.clades
    if ancestors[0].name == "Danio rerio" or ancestors[1].name == "Danio rerio":
        my_measure = min(ancestors[0].branch_length, ancestors[1].branch_length)
    else:
        my_measure = -1
    return my_measure, tree


def read_data(path_to_file: str) -> Tuple[Dict[str, Dict[int, str]], Dict[int, str]]:
    genes = dict()
    species = dict()
    current_gene = ""
    with open(path_to_file, "r") as file:
        for i, line in enumerate(file):
            if i % 8 == 0:
                current_gene = line[:-1]
                genes[current_gene] = dict()
            else:
                species_id, species_name, gene = line[:-1].split('\t')
                species_id = int(species_id)
                genes[current_gene][species_id] = gene
                if i < 8:
                    species[species_id] = species_name
    return genes, species


def main():
    genes, species = read_data("data/genes.txt")
    print("Stay awhile and listen...")
    results = list()
    for gene in tqdm(genes):
        score, tree = upgma(genes[gene], species)
        results.append([score, gene, tree])
    results.sort(reverse=True)

    print("The ranking:")
    for i, result in enumerate(results):
        print(f"{i}.\t{result[1]}\t{result[0]}")

    best_gene_score = results[0][0]
    best_gene_name = results[0][1]
    best_gene_tree = results[0][2]
    worse_gene_score = results[-3][0]
    worse_gene_name = results[-3][1]
    worse_gene_tree = results[-3][2]

    print(f"Best gene is: {best_gene_name}. It achieved {best_gene_score} score.")
    print(f"Worse gene is {worse_gene_name}. It achieved {worse_gene_score} score.")
    axs = plt.axes()
    axs.set_title(f"Best gene: {best_gene_name}")
    axs.set_xlabel("Evolutionary distance")
    axs.set_ylabel("Species")
    draw(best_gene_tree, axes=axs)
    axs2 = plt.axes()
    axs2.set_title(f"Worse gene: {worse_gene_name}")
    axs2.set_xlabel("Evolutionary distance")
    axs2.set_ylabel("Species")
    draw(worse_gene_tree, axes=axs2)


if __name__ == '__main__':
    main()
