import numpy as np


def edit_distance(s: str, t: str) -> int:
    m = len(s) + 1
    n = len(t) + 1
    dynamic_table = np.zeros((m, n))
    for i in range(m):
        for j in range(n):
            if i == 0:
                dynamic_table[i][j] = j
            elif j == 0:
                dynamic_table[i][j] = i
            elif s[i - 1] == t[j - 1]:
                dynamic_table[i][j] = dynamic_table[i - 1][j - 1]
            else:
                dynamic_table[i][j] = 1 + min(dynamic_table[i - 1][j - 1],
                                              dynamic_table[i][j - 1],
                                              dynamic_table[i - 1][j])
    return int(dynamic_table[m-1][n-1])


DNAs = dict()
current_DNA_hlpr = ""
with open('./data/rosalind_edit.txt', 'r') as file:
    for line in file:
        if line[0] == ">":
            current_DNA_hlpr = line[1:-1]
            DNAs[current_DNA_hlpr] = ""
        else:
            DNAs[current_DNA_hlpr] += line[:-1]

DNAs = [value for value in DNAs.values()]
print(edit_distance(DNAs[0], DNAs[1]))
