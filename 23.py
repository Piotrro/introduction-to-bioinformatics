import numpy as np
from Bio.SubsMat.MatrixInfo import blosum62
from typing import Tuple


def scoring_matrix_distance(s: str, t: str, indel_penalty: int = -5) -> Tuple[int, np.ndarray]:
    # s to wiersze, t to kolumny
    s = "-" + s
    t = "-" + t
    m = len(s)
    n = len(t)
    d_t = np.zeros((m, n))
    for i in range(m):
        for j in range(n):
            if i == 0:
                d_t[i][j] = j * indel_penalty
            elif j == 0:
                d_t[i][j] = i * indel_penalty
            else:
                try:
                    blosum_penalty = blosum62[(s[i], t[j])]
                except KeyError:
                    blosum_penalty = blosum62[(t[j], s[i])]
                d_t[i][j] = max(d_t[i-1][j-1] + blosum_penalty,
                                d_t[i-1][j] + indel_penalty,
                                d_t[i][j-1] + indel_penalty)

    return int(d_t[m-1][n-1]), d_t


DNAs = dict()
current_DNA_hlpr = ""
with open('./data/rosalind_glob.txt', 'r') as file:
    for line in file:
        if line[0] == ">":
            current_DNA_hlpr = line[1:-1]
            DNAs[current_DNA_hlpr] = ""
        else:
            DNAs[current_DNA_hlpr] += line[:-1]

DNAs = [value for value in DNAs.values()]
distance, dyn_tbl = scoring_matrix_distance(DNAs[0], DNAs[1])
print(distance)

