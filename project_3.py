from typing import List, Set, Tuple, Dict
from sys import getrecursionlimit, setrecursionlimit


def composition(seq: str, k: int) -> List[str]:
    """
    Creates N k-mers of length k, where N is a number of characters in sequence. It treats seq as a circular genome, so
    last k-mers are build form the combination of last and first characters of the sequence.

    :param seq: genome sequence (circular)
    :param k: size of mers
    :return: list of k-mers
    """
    n = len(seq)
    seq2 = seq + seq[:k]  # seq * 2
    return [seq2[i:i+k] for i in range(n)]


def assembly(comp: List[str], assembled: List[str], found: Set[str]) -> Set[str]:
    """
    Performs genome assembly using k-mers as edges of de Bruijn graph. So it is solving Eulerian path problem for graph
    represented with k-mers ('comp' parameter). Every k-mer can be read as edge between prefix and suffix of the k-mer.
    The algorithm of finding Eulerian path is in-depth recursion.

    :param comp: composition - list of k-mers of genome sequence
    :param assembled: PARAM FOR RECURSION! If you run this function in general purpose (not for some particular test
        case) put empty list here!
    :param found: PARAM FOR RECURSION! If you run this function in general purpose (not for some particular test case)
        put empty set here!
    :return: set of genomes (strings) that might be constructed from given fragments.
    """
    if len(comp) == 0:  # end
        new_genome = reconstruct_from_kmers(assembled)
        found.add(new_genome)
        return found
    elif len(assembled) == 0:  # beginning
        assembled.append(comp[0])
        # print(f"Start with {assembled}")
        found = assembly(comp[1:], assembled, found)
    else:  # global case
        possible_ancestors_ids = [i for i, mer in enumerate(comp) if mer[:-1] == assembled[-1][1:]]
        for index in possible_ancestors_ids:
            assembled.append(comp[index])
            found = assembly(comp[:index] + comp[index+1:], assembled, found)
            del assembled[-1]
    return found


def reconstruct_from_kmers(assembled_kmers: List[str]) -> str:
    """
    Reconstructs genome string from given fragments (in order).
    For example ['ATG', 'TGC', 'GCA', 'CAT'] -> 'GCAT'
    :param assembled_kmers:
    :return:
    """
    return "".join([mer[-1] for mer in assembled_kmers])


def check_assembly(original: str, created: str) -> bool:
    """
    Checks if achieved genome assembly is same as original genome. Products of genome assembly might be different than
    original genome in two cases:
    1. There are loops in de Bruijn graph which are "connected". Example: 'TAATGCCATGGGATGTT' divided into 3-mers will
        produce 'TAATGCCATGGGATGTT' and 'TAATGGGATGCCATGTT' <- here only the first string will be considered as same as
        original genome. For the second string the function will return FALSE
    2. Achieved assembly might be shifted because we do not know from which k-mer should we start. THIS TYPE OF
        DIFFERENCE PRODUCES TRUE AS A RESULT OF THIS FUNCTION! For example: genome 'GCAT' might achieve (3-mer) 'ATGC'
        assembly, which is correct but shifted. The function will return True in such case.
    :param original:
    :param created:
    :return:
    """
    concatenated = created + created
    return original in concatenated


def get_min_k_and_corresponding_assembly(genome: str, k: int = 100) -> Tuple[int, str]:
    """
    Checks for minimum k for building k-mers that will achieve only one unique and correct genome from genome assembly
    operation. It starts from high k and decrease it because the recursive assembly algorithm is very slow for low
    values of k.

    :param genome:
    :param k:
    :return:
    """
    last_created = ""
    while k > 2:
        # print(k)
        comp = composition(genome, k)
        created = assembly(comp, list(), set())
        created = remove_repetitions(created)
        if len(created) > 1:
            return k+1, last_created
        last_created = created[0]
        k -= 1
    return -1, ""


def remove_repetitions(assembled: Set[str]) -> List[str]:
    """
    Some of the assembled genomes might repeat due to loops in the graph. This method founds repetitions and produces
    list with unique assembled genomes.
    For example 'atagctagcg' genome will produce 'ctagcgatag', 'cgatagctag' genomes, which are same (but shifted). The
    function get rid of such repetitions.

    :param assembled:
    :return:
    """
    assembled = list(assembled)
    i = 0
    while i < len(assembled):
        if any([check_assembly(assembled[i], assembled[k]) for k in range(len(assembled)) if i != k]):
            del assembled[i]
        else:
            i += 1
    return assembled


def read_data(path: str) -> Tuple[List[str], List[str]]:
    """
    Reads data from given path. The data should be in format:
    > name1
    ATCGTGCTATGCA
    > name2
    ATGCGTACGAGTAGTCGATC
    ...

    :param path:
    :return:
    """
    names = list()
    genomes = list()
    with open(path, "r") as file:
        for i, line in enumerate(file):
            if i % 2 == 0:
                names.append(line[2:-1])
            else:
                genomes.append(line[:-1])
    return names, genomes


def save_results(path: str, answer: Dict[str, int]) -> None:
    s = ""
    for name, k in answer.items():
        s += f"{name}\t{k}\n"
    with open(path, "w") as file:
        file.write(s)


def main():
    names, genomes = read_data("data/genomes.txt")
    answer = dict()
    setrecursionlimit(getrecursionlimit() * 2)
    for i in range(len(names)):
        k, created = get_min_k_and_corresponding_assembly(genomes[i])
        answer[names[i]] = k
        print(f"Genome {names[i]}, min k = {k}, assembly check = {check_assembly(genomes[i], created)}")
    save_results("output/project_3_out.txt", answer)


main()
