import re
from typing import List


def get_k_mers(k: int, alphabet: str) -> List[str]:
    result = list()
    alp_len = len(alphabet)
    for i in range(alp_len ** k):
        tmp_str = ""
        for j in range(k):
            tmp_str += alphabet[int(i / alp_len ** j) % alp_len]
        result.append(tmp_str[::-1])  # reverse to keep alphabetical order
    return result


def count_occurs_in_string(substring: str, string: str) -> int:
    return len([match.start() + 1 for match in re.finditer(f'(?={substring})', string)])


DNAs = dict()
current_DNA_hlpr = ""
with open('./data/rosalind_kmer.txt', 'r') as file:
    for line in file:
        if line[0] == ">":
            current_DNA_hlpr = line[1:-1]
            DNAs[current_DNA_hlpr] = ""
        else:
            DNAs[current_DNA_hlpr] += line[:-1]

alphabet = "ACGT"
k = 4
s = DNAs[next(iter(DNAs))]
all_mers = get_k_mers(k, alphabet)
numbers_of_occurs = [count_occurs_in_string(mer, s) for mer in all_mers]

for number in numbers_of_occurs:
    print(number, end=" ")

