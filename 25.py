from Bio.SubsMat.MatrixInfo import blosum62
from Bio import pairwise2

DNAs = dict()
current_DNA_hlpr = ""
with open('./data/rosalind_gaff.txt', 'r') as file:
    for line in file:
        if line[0] == ">":
            current_DNA_hlpr = line[1:-1]
            DNAs[current_DNA_hlpr] = ""
        else:
            DNAs[current_DNA_hlpr] += line[:-1]

DNAs = [value for value in DNAs.values()]

align = pairwise2.align.globalds(DNAs[0], DNAs[1], blosum62, -11, -1)[0]

print(int(align[2]))
print(align[0])
print(align[1])
