def hamming_distance(s, t):
    counter = 0
    for i in range(len(s)):
        if s[i] != t[i]:
            counter += 1
    return counter


with open("./data/rosalind_hamm.txt", "r") as file:
    s = file.readline()[:-1]
    t = file.readline()[:-1]

print(hamming_distance(s, t))
