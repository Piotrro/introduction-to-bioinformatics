s = ""
with open('./data/rosalind_ini5.txt', 'r') as file:
    for i, line in enumerate(file):
        if i % 2 == 1:
            s += line

with open('./output/out_5.txt', 'w') as out_file:
    out_file.write(s)
