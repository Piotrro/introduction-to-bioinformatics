from Bio import Entrez
from Bio import SeqIO
from itertools import chain
from matplotlib import pyplot as plt
from statistics import median
from tqdm import tqdm
from typing import List, Tuple


def revert_genome(sequence: str) -> str:
    """
    Changes the genome sequence to its "second string" by reverting the order of nucleotides, and then changing each
    nucleotide to its "mirror brother".
    :param sequence: string describing genome in ACGT alphabet.
    :return: string representing the "second string" of DNA, in ACGT alphabet.
    """
    reverted = sequence[::-1]
    reverted = reverted.replace("G", "c").replace("C", "g").replace("A", "t").replace("T", "a").upper()
    return reverted


def generate_codon_sequences(sequence: str) -> List[List[str]]:
    """
    Divides the genome sequence into codons (parts of genome made of 3 nucleotides). It produces 3 lists as we do not
    know the start position of gene in genome. So for example ATAGCTA will be divided into ['ATA', 'GCT'] and
    ['TAG', 'CTA'] and ['AGC'].
    :param sequence: string describing genome.
    :return: list of lists containing codons as strings.
    """
    result = list()
    for j in range(3):
        result.append([sequence[i+j:i+3+j] for i in range(0, len(sequence), 3)])
        if len(result[j][-1]) < 3:
            del result[j][-1]
    return result


def find_genes_in_codons_sequence(codon_sequence: List[str]) -> List[List[int]]:
    """
    Finds genes in sequence of codons. The genes might not be real genes - these are rather "candidates for genes" as
    they can be too short... Basically the function looks for codon encoding "start" of genome, and next "stop" encoding
    codons.
    :param codon_sequence: list of lists containing codons as strings.
    :return: list of list. Each inner list is a pair of two integers which stands for (start, end) coordinates of gene.
    """
    result = list()
    for position, codon in enumerate(codon_sequence):
        if codon == "ATG":  # if the codon is a "start"
            tmp_start = position
            tmp_end = -1
            for i, possible_end_codon in enumerate(codon_sequence[tmp_start:]):
                if possible_end_codon in ["TAA", "TAG"]:  # if the codon is "end"
                    tmp_end = i + tmp_start + 1
                    break
            if tmp_end != -1:
                result.append([tmp_start, tmp_end])
    return result


def filter_genes(genes_positions: List[List[int]], min_len: int) -> List[List[int]]:
    """
    Filters out too short genes, keeping only ones which are longer then min_len value.
    :param genes_positions: list of list. Each inner list is a pair of two integers which stands for
    (start, end) coordinates of gene.
    :param min_len: minimal lenght of genome
    :return: list of list. Each inner list is a pair of two integers which stands for (start, end) coordinates of gene.
    """
    result = list()
    for gene_position in genes_positions:
        if gene_position[1] - gene_position[0] >= min_len:
            result.append(gene_position)
    return result


def convert_to_real_loc(list_of_codon_locations: List[List[int]], shift: int) -> List[List[int]]:
    """
    Changes location of codon into location of nucleotide in genome
    :param list_of_codon_locations:
    :param shift:
    :return:
    """
    return [[pair[0] * 3 + shift, pair[1] * 3 + shift] for pair in list_of_codon_locations]


def calculate_precision_and_recall(fward: List[List[int]], bward: List[List[int]],
                                   real_fward: List[List[int]], real_bward: List[List[int]]) -> Tuple[float, float]:
    """
    Calculates precision and recall for both strings of DNA (together, at once).
    :param fward: Observed results mRNA.
    :param bward: Observed results second string.
    :param real_fward: Real values mRNA.
    :param real_bward: Real values second string.
    :return:
    """
    number_of_real_genes = len(real_fward) + len(real_bward)  # (521) FN + TP
    tp = 0
    fp = 0
    for position in fward:
        if position in real_fward:
            tp += 1
        else:
            fp += 1
    for position in bward:
        if position in real_bward:
            tp += 1
        else:
            fp += 1
    prec = tp / (tp + fp)
    reca = tp / number_of_real_genes
    return prec, reca


def calculate_and_print_statistics(real_fward: List[List[int]], real_bward: List[List[int]]) -> None:
    """
    The function calculates and prints answers to question no. 3 of the exercise.
    :param real_fward:
    :param real_bward:
    :return:
    """
    lengths = [int((pos[1] - pos[0]) / 3) for pos in real_fward] + [int((pos[1] - pos[0]) / 3) for pos in real_bward]
    lengths.sort()
    minimum = lengths[0]
    maximum = lengths[-1]
    med = median(lengths)
    print(f"Length of smallest gene = {minimum} (codons)\n"
          f"Length of longest gene = {maximum} (codons)\n"
          f"Median length of all genes = {med} (codons).")


# Parameters
Entrez.email = "pg5179@student.uni-lj.si"
id_of_organism = "NC_000908"
# L = 50  # Minimal number of codons
L_start = 0
L_end = 550

# Variables
print("Getting data...")
with Entrez.efetch(db="nucleotide", id=id_of_organism, rettype="fasta", retmode="text") as handle:
    data = SeqIO.read(handle, "fasta")
genome_seq = str(data.seq)  # length = 580076
with Entrez.efetch(db="nucleotide", rettype="gb", id=id_of_organism) as handle:
    rec = SeqIO.read(handle, "gb")
real_genes_loc_forward = [[int(feature.location.start), int(feature.location.end)] for feature in rec.features
                          if feature.type == "CDS" and feature.location.strand == 1]
real_genes_loc_backward = [[int(feature.location.start), int(feature.location.end)] for feature in rec.features
                           if feature.type == "CDS" and feature.location.strand == -1]
# calculate_and_print_statistics(real_genes_loc_forward, real_genes_loc_backward)  # q3 answer

print("Looking for 'candidates' for genes in genome...")
codon_seqs = generate_codon_sequences(genome_seq) + generate_codon_sequences(revert_genome(genome_seq))
possible_genes_loc_codon = [find_genes_in_codons_sequence(codon_sequence) for codon_sequence in codon_seqs]

precisions = list()
recalls = list()
print("Thresholding genes...")
for L in tqdm(range(L_start, L_end, 1)):
    genes_loc_codon = [filter_genes(possible_genes, L) for possible_genes in possible_genes_loc_codon]
    genes_loc_forward = [convert_to_real_loc(locs_codon, i) for i, locs_codon in enumerate(genes_loc_codon[:3])]
    genes_loc_forward = list(chain.from_iterable(genes_loc_forward))
    genes_loc_backward = [convert_to_real_loc(locs_codon, i) for i, locs_codon in enumerate(genes_loc_codon[3:])]
    genes_loc_backward = list(chain.from_iterable(genes_loc_backward))

    precision, recall = calculate_precision_and_recall(genes_loc_forward, genes_loc_backward, real_genes_loc_forward,
                                                       real_genes_loc_backward)
    precisions.append(precision)
    recalls.append(recall)

plt.plot(range(L_start, L_end, 1), precisions, 'g-', label="Precision")
plt.plot(range(L_start, L_end, 1), recalls, 'r-', label="Recall")
plt.ylabel("Precision / Recall")
plt.xlabel("L")
plt.legend()
plt.show()
